
[F-Droid](https://gitlab.com/fdroid/fdroidclient) can optionally keep
a log on install and uninstall activity.  This is an example project
for reading that log from an external app.  The logging is controlled
by the preference called "Keep install history" aka
`keepInstallHistory`.

This is currently only useful when making custom
[white label builds](https://f-droid.org/wiki/page/Whitelabel_Builds)
of F-Droid, since the external app must have be compiled in before
F-Droid will grant read access.  In order to set that, edit the string
called `install_history_reader_packageName` in _donottranslate.xml_.
