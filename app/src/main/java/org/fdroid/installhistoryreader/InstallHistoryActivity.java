/*
 * Copyright (C) 2016 Blue Jay Wireless
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

package org.fdroid.installhistoryreader;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.apache.commons.io.IOUtils;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;

public class InstallHistoryActivity extends AppCompatActivity {
    public static final String TAG = "InstallHistoryActivity";

    public static final String AUTHORITY = "org.fdroid.fdroid.installer";
    public static final Uri LOG_URI = Uri.parse("content://" + AUTHORITY + "/install_history/all");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_install_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = "";
                String snackBarText = "";
                try {
                    ContentResolver resolver = getContentResolver();

                    Cursor cursor = resolver.query(LOG_URI, null, null, null, null);
                    if (cursor != null) {
                        int sizeColumn = cursor.getColumnIndex(OpenableColumns.SIZE);
                        cursor.moveToFirst();
                        snackBarText = "read in " + cursor.getLong(sizeColumn)
                                + " bytes from " + LOG_URI;
                        Log.i(TAG, snackBarText);
                        cursor.close();
                    }

                    ParcelFileDescriptor pfd = resolver.openFileDescriptor(LOG_URI, "r");
                    if (pfd == null) {
                        snackBarText = "null ParcelFileDescriptor";
                        Log.i(TAG, snackBarText);
                        throw new IllegalStateException(snackBarText);
                    }
                    FileDescriptor fd = pfd.getFileDescriptor();
                    FileInputStream fileInputStream = new FileInputStream(fd);
                    text = IOUtils.toString(fileInputStream, Charset.defaultCharset());
                } catch (IOException | SecurityException | IllegalStateException e) {
                    snackBarText = e.getMessage();
                    e.printStackTrace();
                }
                Snackbar.make(view, snackBarText, Snackbar.LENGTH_INDEFINITE).show();
                TextView textView = (TextView) findViewById(R.id.text);
                textView.setText(text);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_delete:
                getContentResolver().delete(LOG_URI, null, null);
                View view = findViewById(R.id.toolbar_layout);
                Snackbar.make(view, "deleted history", Snackbar.LENGTH_INDEFINITE).show();
                TextView textView = (TextView) findViewById(R.id.text);
                textView.setText("");
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
